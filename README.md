# arithmetic Operator

Apply an arithmetic operation between the first 2 TS of the input.  
If more than 2 TS are present, the first 2 are used.  
Pay attention to the order for non-commutative operation: `TS[0] - TS[1]`

## Input and parameters

This operator takes 1 input:

- **TS list** (*ts_list*): List of TS

This operator takes 1 parameter:

- **Operation** (*list*): Arithmetic operator to apply. Currently `+`, `-`, `*`, `/`

## Outputs

This operator has 1 output:

- **TS list** (*ts_list*): TS resulting of the arithmetic operation as a ts_list (of 1 TS)

## Principle

The operator uses `numpy` to do the arithmetic operation

## Warnings/Limitations

The operator doesn't scale. Don't try to apply it on large Timeseries