"""
Copyright 2018-2019 CS Systèmes d'Information

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

"""

import os, sys
import numpy as np

# Import IKATS Api
from ikats import IkatsAPI
from ikats.exceptions import *
from ikats.extra.timeseries import *
from ikats.lib import MDType

class operator:
    def __init__(self, *args, **kwargs):
        return super().__init__(*args, **kwargs)
    def check(**kwargs):
        """
        Check the parameters are valid
        """
        pass
    def run(ts_list, result_fid, operator="add"):
        """
        Execute the operator using IKATS context
        """

        # Create the API instance
        api = IkatsAPI()

        # First TS of the dataset
        ts1 = api.ts.get(fid=ts_list[0]['funcId'])
        # Second
        ts2 = api.ts.get(fid=ts_list[1]['funcId'])

        result_fid_filled = result_fid.format(
            **{
                "TS1": ts_list[0]['funcId'],
                "TS2": ts_list[1]['funcId'],
                "op": operator
            })

        # Linear combinations between TS
        # value of the sum = sum(valueTS1, valueTS2)
        if operator == "add":
            result_values = ts1.data[:, 1] + ts2.data[:, 1]
        elif operator == "sub":
            result_values = ts1.data[:, 1] - ts2.data[:, 1]
        elif operator == "mul":
            result_values = ts1.data[:, 1] * ts2.data[:, 1]
        elif operator == "div":
            result_values = ts1.data[:, 1] / ts2.data[:, 1]
        else:
            raise ValueError("Bad operator %s"%operator)

        data = list(zip([int(x) for x in ts1.data[:, 0]], result_values))
        ts_result = api.ts.new(fid=result_fid_filled, data=data)

        # Save
        # Now we save the TS and request to generate minimum metadata (ikats_start_date, ikats_end_date, qual_nb_points)
        ts_result.save(generate_metadata=True)

        return [{"tsuid": ts_result.tsuid, "funcId": ts_result.fid}]


def main(*args,**kwargs):
    return operator.run(*args,**kwargs)